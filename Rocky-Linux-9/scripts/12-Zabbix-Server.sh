#!/bin/bash

# 12-Zabbix Server

if [ "$zabbix_option" = "1" ];then # 1-) Zabbix Server 6.0 LTS (MySQL & Apache)
    printf "Please Enter Desired MySQL Password:"
    read -r mysqlpassword
    grep -q "^excludepkgs=zabbix*" /etc/yum.repos.d/epel.repo || echo "excludepkgs=zabbix*" | sudo tee -a /etc/yum.repos.d/epel.repo > /dev/null
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/9/x86_64/zabbix-release-latest-6.0.el9.noarch.rpm
    sudo dnf clean all
    sudo dnf -vy install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-sql-scripts \
    zabbix-selinux-policy zabbix-agent httpd httpd-devel php-fpm
    sudo systemctl start mysqld
    sudo systemctl enable mysqld
    sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$mysqlpassword';"
    sudo systemctl restart mysqld.service
    sudo mysql -uroot -p"$mysqlpassword" -e "create database zabbix character set utf8 collate utf8_bin;"
    sudo mysql -uroot -p"$mysqlpassword" -e "create user zabbix@localhost identified by '$mysqlpassword';"
    sudo mysql -uroot -p"$mysqlpassword" -e "grant all privileges on zabbix.* to zabbix@localhost;"
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 1;"
    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p"$mysqlpassword" zabbix
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 0;"
    sed -i "s/# DBPassword=/DBPassword=$mysqlpassword/g" /etc/zabbix/zabbix_server.conf
    sudo systemctl restart zabbix-server zabbix-agent httpd php-fpm
    sudo systemctl enable zabbix-server zabbix-agent httpd php-fpm
elif [ "$zabbix_option" = "2" ];then # 2-) Zabbix Server 6.0 LTS (Mysql & Nginx)
    printf "Please Enter Desired Mysql Password:"
    read -r mysqlpassword
    grep -q "^excludepkgs=zabbix*" /etc/yum.repos.d/epel.repo || echo "excludepkgs=zabbix*" | sudo tee -a /etc/yum.repos.d/epel.repo > /dev/null
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/9/x86_64/zabbix-release-latest-6.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-mysql zabbix-web-mysql zabbix-nginx-conf zabbix-sql-scripts \
    zabbix-selinux-policy zabbix-agent nginx php-fpm
    sudo systemctl start mysqld
    sudo systemctl enable mysqld
    sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$mysqlpassword';"
    sudo systemctl restart mysqld.service
    sudo mysql -uroot -p"$mysqlpassword" -e "create database zabbix character set utf8 collate utf8_bin;"
    sudo mysql -uroot -p"$mysqlpassword" -e "create user zabbix@localhost identified by '$mysqlpassword';"
    sudo mysql -uroot -p"$mysqlpassword" -e "grant all privileges on zabbix.* to zabbix@localhost;"
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 1;"
    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p"$mysqlpassword" zabbix
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 0;"
    sed -i "s/# DBPassword=/DBPassword=$mysqlpassword/g" /etc/zabbix/zabbix_server.conf
    printf "Please Enter Desired Nginx Port:"
    read -r nginx_port
    sudo sed -i "s/#\s*listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    printf "Please Enter Desired Server Name:"
    read -r server_name
    sudo sed -i "s/#\s*server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo systemctl restart zabbix-server zabbix-agent nginx php-fpm
    sudo systemctl enable zabbix-server zabbix-agent nginx php-fpm
elif [ "$zabbix_option" = "3" ];then # 3-) Zabbix Server 6.0 LTS (PostgreSQL & Apache)
    printf "Please Enter Desired MySQL Password:"
    read -r pgpassword
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/9/x86_64/zabbix-release-latest-6.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-pgsql zabbix-web-pgsql zabbix-apache-conf zabbix-sql-scripts \
    zabbix-selinux-policy zabbix-agent httpd httpd-devel php-fpm
    printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL 17\n2-)PostgreSQL 16\n\
3-)PostgreSQL 15\n4-)PostgreSQL 14\n5-)PostgreSQL 13\n\nPlease Select Your PostgreSQL Version:"
    read -r postgresql_version
    sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    sudo dnf -vy module disable postgresql
    sudo dnf clean all
    if [ "$postgresql_version" = "1" ];then # 1-)PostgreSQL 17
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql17-server
        sudo /usr/pgsql-17/bin/postgresql-17-setup initdb
        sudo systemctl enable postgresql-17
        sudo systemctl start postgresql-17
    elif [ "$postgresql_version" = "2" ];then # 2-)PostgreSQL 16
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql16-server
        sudo /usr/pgsql-16/bin/postgresql-16-setup initdb
        sudo systemctl enable postgresql-16
        sudo systemctl start postgresql-16
    elif [ "$postgresql_version" = "3" ];then # 3-)PostgreSQL 15
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql15-server
        sudo /usr/pgsql-15/bin/postgresql-15-setup initdb
        sudo systemctl enable postgresql-15
        sudo systemctl start postgresql-15
    elif [ "$postgresql_version" = "4" ];then # 4-)PostgreSQL 14
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql14-server
        sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
        sudo systemctl enable postgresql-14
        sudo systemctl start postgresql-14
    elif [ "$postgresql_version" = "5" ];then # 5-)PostgreSQL 13
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql13-server
        sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
        sudo systemctl enable postgresql-13
        sudo systemctl start postgresql-13
    else
        echo "Out of options please choose between 1-5"
    fi
    echo "Please Enter Your Zabbix User Password"
    sudo -u postgres createuser --pwprompt zabbix
    sudo -u postgres createdb -O zabbix zabbix
    zcat /usr/share/doc/zabbix-server-pgsql*/create.sql.gz | sudo -u zabbix psql zabbix
    sed -i "s/DBPassword=/DBPassword=$pgpassword/g" /etc/zabbix/zabbix_server.conf
    sudo systemctl restart zabbix-server zabbix-agent httpd
    sudo systemctl enable zabbix-server zabbix-agent httpd
elif [ "$zabbix_option" = "4" ];then # 4-) Zabbix Server 6.0 LTS (PostgreSQL & Nginx)
    printf "Please Enter Desired PostgreSQL Password:"
    read -r pgpassword
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/9/x86_64/zabbix-release-latest-6.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-pgsql zabbix-web-pgsql zabbix-nginx-conf \
    zabbix-sql-scripts zabbix-selinux-policy zabbix-agent nginx php-fpm
    printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL 17\n2-)PostgreSQL 16\n\
3-)PostgreSQL 15\n4-)PostgreSQL 14\n5-)PostgreSQL 13\n\nPlease Select Your PostgreSQL Version:"
    read -r postgresql_version
    sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    sudo dnf -vy module disable postgresql
    sudo dnf clean all
    if [ "$postgresql_version" = "1" ];then # 1-)PostgreSQL 17
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql17-server
        sudo /usr/pgsql-17/bin/postgresql-17-setup initdb
        sudo systemctl enable postgresql-17
        sudo systemctl start postgresql-17
    elif [ "$postgresql_version" = "2" ];then # 2-)PostgreSQL 16
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql16-server
        sudo /usr/pgsql-16/bin/postgresql-16-setup initdb
        sudo systemctl enable postgresql-16
        sudo systemctl start postgresql-16
    elif [ "$postgresql_version" = "3" ];then # 3-)PostgreSQL 15
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql15-server
        sudo /usr/pgsql-15/bin/postgresql-15-setup initdb
        sudo systemctl enable postgresql-15
        sudo systemctl start postgresql-15
    elif [ "$postgresql_version" = "4" ];then # 4-)PostgreSQL 14
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql14-server
        sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
        sudo systemctl enable postgresql-14
        sudo systemctl start postgresql-14
    elif [ "$postgresql_version" = "5" ];then # 5-)PostgreSQL 13
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql13-server
        sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
        sudo systemctl enable postgresql-13
        sudo systemctl start postgresql-13
    else
        echo "Out of options please choose between 1-5"
    fi
    echo "Please Enter Your Zabbix User Password"
    sudo -u postgres createuser --pwprompt zabbix
    sudo -u postgres createdb -O zabbix zabbix
    zcat /usr/share/zabbix-sql-scripts/postgresql/server.sql.gz | sudo -u zabbix psql zabbix
    sed -i "s/DBPassword=/DBPassword=$pgpassword/g" /etc/zabbix/zabbix_server.conf
        printf "Please Enter Desired Nginx Port:"
    read -r nginx_port
    sudo sed -i "s/#\s*listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    printf "Please Enter Desired Server Name:"
    read -r server_name
    sudo sed -i "s/#\s*server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo systemctl restart zabbix-server zabbix-agent nginx
    sudo systemctl enable zabbix-server zabbix-agent nginx    
elif [ "$zabbix_option" = "5" ];then # 5-) Zabbix Server 7.0 LTS (Mysql & Apache)
    printf "Please Enter Desired MySQL Password:"
    read -r mysqlpassword
    grep -q "^excludepkgs=zabbix*" /etc/yum.repos.d/epel.repo || echo "excludepkgs=zabbix*" | sudo tee -a /etc/yum.repos.d/epel.repo > /dev/null
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/7.0/rocky/9/x86_64/zabbix-release-latest-7.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-sql-scripts zabbix-selinux-policy zabbix-agent \
    httpd httpd-devel php-fpm
    sudo systemctl start mysqld
    sudo systemctl enable mysqld
    sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$mysqlpassword';"
    sudo systemctl restart mysqld.service
    sudo mysql -uroot -p"$mysqlpassword" -e "create database zabbix character set utf8mb4 collate utf8mb4_bin;"
    sudo mysql -uroot -p"$mysqlpassword" -e "create user zabbix@localhost identified by '$mysqlpassword';"
    sudo mysql -uroot -p"$mysqlpassword" -e "grant all privileges on zabbix.* to zabbix@localhost;"
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 1;"
    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p"$mysqlpassword" zabbix
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 0;"
    sed -i "s/# DBPassword=/DBPassword=$mysqlpassword/g" /etc/zabbix/zabbix_server.conf
    sudo systemctl restart zabbix-server zabbix-agent httpd php-fpm
    sudo systemctl enable zabbix-server zabbix-agent httpd php-fpm
elif [ "$zabbix_option" = "6" ];then # 6-) Zabbix Server 7.0 LTS (Mysql & Nginx)
    printf "Please Enter Desired MySQL Password:"
    read -r mysqlpassword
    grep -q "^excludepkgs=zabbix*" /etc/yum.repos.d/epel.repo || echo "excludepkgs=zabbix*" | sudo tee -a /etc/yum.repos.d/epel.repo > /dev/null
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/7.0/rocky/9/x86_64/zabbix-release-latest-7.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-mysql zabbix-web-mysql zabbix-nginx-conf zabbix-sql-scripts \
    zabbix-selinux-policy zabbix-agent nginx php-fpm
    sudo systemctl start mysqld
    sudo systemctl enable mysqld
    sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$mysqlpassword';"
    sudo systemctl restart mysqld.service
    sudo mysql -uroot -p"$mysqlpassword" -e "create database zabbix character set utf8mb4 collate utf8mb4_bin;"
    sudo mysql -uroot -p"$mysqlpassword" -e "create user zabbix@localhost identified by '$mysqlpassword';"
    sudo mysql -uroot -p"$mysqlpassword" -e "grant all privileges on zabbix.* to zabbix@localhost;"
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 1;"
    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p"$mysqlpassword" zabbix
    sudo mysql -uroot -p"$mysqlpassword" -e "set global log_bin_trust_function_creators = 0;"
    sed -i "s/# DBPassword=/DBPassword=$mysqlpassword/g" /etc/zabbix/zabbix_server.conf
    printf "Please Enter Desired Nginx Port:"
    read -r nginx_port
    sudo sed -i "s/#\s*listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    printf "Please Enter Desired Server Name:"
    read -r server_name
    sudo sed -i "s/#\s*server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo systemctl restart zabbix-server zabbix-agent nginx php-fpm
    sudo systemctl enable zabbix-server zabbix-agent nginx php-fpm
elif [ "$zabbix_option" = "7" ];then # 7-) Zabbix Server 7.0 LTS (PostgreSQL & Apache)
    printf "Please Enter Desired PostgreSQL Password:"
    read -r pgpassword
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/7.0/rocky/9/x86_64/zabbix-release-latest-7.0.el9.noarch.rpm
    sudo dnf -v clean all
    sudo dnf -vy install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-sql-scripts \
    zabbix-selinux-policy zabbix-agent mysql mysql-devel mysql-server httpd httpd-devel
    printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL 17\n2-)PostgreSQL 16\n\
3-)PostgreSQL 15\n4-)PostgreSQL 14\n5-)PostgreSQL 13\n\nPlease Select Your PostgreSQL Version:"
    read -r postgresql_version
    sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    sudo dnf -vy module disable postgresql
    sudo dnf clean all
    if [ "$postgresql_version" = "1" ];then # 1-)PostgreSQL 17
        sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf -vy install postgresql17-server
        sudo /usr/pgsql-17/bin/postgresql-17-setup initdb
        sudo systemctl enable postgresql-17
        sudo systemctl start postgresql-17
    elif [ "$postgresql_version" = "2" ];then # 2-)PostgreSQL 16
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql16-server
        sudo /usr/pgsql-16/bin/postgresql-16-setup initdb
        sudo systemctl enable postgresql-16
        sudo systemctl start postgresql-16
    elif [ "$postgresql_version" = "3" ];then # 3-)PostgreSQL 15
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql15-server
        sudo /usr/pgsql-15/bin/postgresql-15-setup initdb
        sudo systemctl enable postgresql-15
        sudo systemctl start postgresql-15
    elif [ "$postgresql_version" = "4" ];then # 4-)PostgreSQL 14
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql14-server
        sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
        sudo systemctl enable postgresql-14
        sudo systemctl start postgresql-14
    elif [ "$postgresql_version" = "5" ];then # 5-)PostgreSQL 13
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql13-server
        sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
        sudo systemctl enable postgresql-13
        sudo systemctl start postgresql-13
    else
        echo "Out of options please choose between 1-5"
    fi
    zcat /usr/share/doc/zabbix-sql-scripts/mysql/server.sql.gz | mysql -uzabbix -p"$mysqlpassword" zabbix
    sed -i "s/# DBPassword=/DBPassword=$mysqlpassword/g" /etc/zabbix/zabbix_server.conf
    sudo systemctl restart zabbix-server zabbix-agent httpd php-fpm
    sudo systemctl enable zabbix-server zabbix-agent httpd php-fpm
elif [ "$zabbix_option" = "8" ];then # 8-) Zabbix Server 7.0 LTS (PostgreSQL & Nginx)
    printf "Please Enter Desired PostgreSQL Password:"
    read -r pgpassword
    sudo dnf -vy remove zabbix-release
    sudo rpm -Uvh https://repo.zabbix.com/zabbix/7.0/rocky/9/x86_64/zabbix-release-latest-7.0.el9.noarch.rpm
    dnf clean all
    sudo dnf -vy install zabbix-server-pgsql zabbix-web-pgsql zabbix-nginx-conf \
    zabbix-sql-scripts zabbix-selinux-policy zabbix-agent nginx php-fpm
    printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL 17\n2-)PostgreSQL 16\n\
3-)PostgreSQL 15\n4-)PostgreSQL 14\n5-)PostgreSQL 13\n\nPlease Select Your PostgreSQL Version:"
    read -r postgresql_version
    sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    sudo dnf -vy module disable postgresql
    sudo dnf clean all
    if [ "$postgresql_version" = "1" ];then # 1-)PostgreSQL 17
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql17-server
        sudo /usr/pgsql-17/bin/postgresql-17-setup initdb
        sudo systemctl enable postgresql-17
        sudo systemctl start postgresql-17
    elif [ "$postgresql_version" = "2" ];then # 2-)PostgreSQL 16
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql16-server
        sudo /usr/pgsql-16/bin/postgresql-16-setup initdb
        sudo systemctl enable postgresql-16
        sudo systemctl start postgresql-16
    elif [ "$postgresql_version" = "3" ];then # 3-)PostgreSQL 15
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql15-server
        sudo /usr/pgsql-15/bin/postgresql-15-setup initdb
        sudo systemctl enable postgresql-15
        sudo systemctl start postgresql-15
    elif [ "$postgresql_version" = "4" ];then # 4-)PostgreSQL 14
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql14-server
        sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
        sudo systemctl enable postgresql-14
        sudo systemctl start postgresql-14
    elif [ "$postgresql_version" = "5" ];then # 5-)PostgreSQL 13
        sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        sudo dnf -qy module disable postgresql
        sudo dnf install -y postgresql13-server
        sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
        sudo systemctl enable postgresql-13
        sudo systemctl start postgresql-13
    else
        echo "Out of options please choose between 1-5"
    fi
    echo "Please Enter Your Zabbix User Password"
    sudo -u postgres createuser --pwprompt zabbix
    sudo -u postgres createdb -O zabbix zabbix
    zcat /usr/share/zabbix-sql-scripts/postgresql/server.sql.gz | sudo -u zabbix psql zabbix
    sed -i "s/DBPassword=/DBPassword=$pgpassword/g" /etc/zabbix/zabbix_server.conf
        printf "Please Enter Desired Nginx Port:"
    read -r nginx_port
    sudo sed -i "s/#\s*listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/listen\s\+[0-9]\{1,\};/listen $nginx_port;/" /etc/nginx/conf.d/zabbix.conf
    printf "Please Enter Desired Server Name:"
    read -r server_name
    sudo sed -i "s/#\s*server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo sed -i "s/server_name\s\+.*;/server_name $server_name;/" /etc/nginx/conf.d/zabbix.conf
    sudo systemctl restart zabbix-server zabbix-agent nginx
    sudo systemctl enable zabbix-server zabbix-agent nginx
else
    echo "Out of options please choose between 1-10"
fi