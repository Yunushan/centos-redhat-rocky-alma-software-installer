#!/bin/bash

# 23-Kibana

read -r kibana_version
if [ "$kibana_version" = "1" ];then # 1-) Kibana (with repository package manager)
    sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
    echo "[kibana-8.x]
name=Kibana repository for 8.x packages
baseurl=https://artifacts.elastic.co/packages/8.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md" > /etc/yum.repos.d/kibana.repo
    sudo dnf -vy install kibana
    sudo systemctl daemon-reload
    sudo systemctl enable kibana
    sudo systemctl start kibana
elif [ "$kibana_version" = "2" ];then # 2-) Kibana (Via Docker)
    sudo dnf -vy install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
    sudo dnf -vy install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo systemctl enable --now docker
    systemctl start docker
    docker network create elastic
    elasticsearch_docker=$(lynx -dump https://www.elastic.co/guide/en/kibana/current/docker.html \
    | grep -i "docker pull docker.elastic.co/elasticsearch/elasticsearch" | grep -iv "wolfi" | head -n 1)
    eval "$elasticsearch_docker"
    lynx -dump https://www.elastic.co/guide/en/kibana/current/docker.html | grep -i "ttps://artifacts.elastic.co/cosign.pub" | head -n 1
elif [ "$kibana_version" = "3" ];then # 3-) Kibana (From .rpm file)
    kibana_rpm=$(lynx -dump https://www.elastic.co/guide/en/kibana/current/rpm.html \
    | grep -i "https://artifacts.elastic.co/downloads/kibana/" | grep -iv ".sha" | head -n 1)
    kibana_rpm_version=$(lynx -dump https://www.elastic.co/guide/en/kibana/current/rpm.html \
    | grep -i "https://artifacts.elastic.co/downloads/kibana/" | grep -iv ".sha" | head -n 1 | sed -E 's/.*kibana-([0-9]+\.[0-9]+\.[0-9]+).*\.rpm/\1/')
    wget "$kibana_rpm" /root/Donwloads/kibana
    rpm -Uvh "$kibana_rpm"
else
    echo "Out of options please choose between 1-3"
fi