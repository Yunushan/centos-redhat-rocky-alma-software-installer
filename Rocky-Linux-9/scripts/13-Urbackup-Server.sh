#!/bin/bash

# 13-UrBackup Server

if [ "$urbackup_version" = "1" ];then # 1-)UrBackup Server Latest (Compile From Source)
    sudo dnf -vy install gcc gcc-c++ zlib zlib-devel libcurl libcurl-devel openssl-devel cryptopp-devel
    urbackup_latest=$(lynx -dump https://hndl.urbackup.org/Server/latest/ | awk '/http/{print $2}' | grep -iv ".deb" | grep -i ".tar.gz" | head -n 1)
    sudo mkdir -pv /root/Downloads/urbackup-server-latest
    wget -O /root/Downloads/urbackup-server-latest.tar.gz "$urbackup_latest"
    tar -xvf /root/Downloads/urbackup-server-latest.tar.gz -C /root/Downloads/urbackup-server-latest --strip-components 1
    cd /root/Downloads/urbackup-server-latest || exit 2
    ./configure
    make -j "${core:=}" && make -j "${core:=}" install
    cp urbackup-server.service /etc/systemd/system/
    systemctl enable urbackup-server.service
    cp defaults_server /etc/default/urbackupsrv
    cp logrotate_urbackupsrv /etc/logrotate.d/urbackupsrv
    systemctl start urbackup-server
elif [ "$urbackup_version" = "2" ];then # 2-)UrBackup Server (Docker)
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
    sudo dnf -vy install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    systemctl start docker
    systemctl enable docker
    docker run -d --name urbackup-server-1 -v /media/backups:/backups -v /media/database:/var/urbackup -p \
    55413-55415:55413-55415 -p 35623:35623/udp uroni/urbackup-server
    echo "Out of options please choose between 1-2"
fi