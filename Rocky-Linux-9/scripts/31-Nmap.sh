#!/bin/bash

# 31-Nmap

if [ "$nmap_version" = "1" ];then # 1-) Nmap (From Official Repository)
    sudo dnf -vy install nmap
elif [ "$nmap_version" = "2" ];then # 2-) Nmap Latest(From .rpm File)
    nmap_rpm=$(lynx -dump https://nmap.org/download | awk '/http/{print $2}' | grep -i "x86_64.rpm" | grep -i "nmap-" | head -n 1)
    rpm -Uvh $nmap_rpm
elif [ "$nmap_version" = "3" ];then # 3-) Nmap Latest (Compile From Source)
    nmap_bz2=$(lynx -dump https://nmap.org/download | awk '/http/{print $2}' | grep -i ".tar.bz2" | head -n 1)
    sudo wget -O /root/Downloads/nmap_latest.bz2 "$nmap_bz2"
    bzip2 -cd nmap_latest.bz2 | tar xvf -
    cd nmap_latest
    ./configure
    make -j "$core"
    make install
else
    echo "Out of option Please Choose between 1-3"
fi