#!/bin/bash

# 22-Logstash

if [ "$logstash_version" = "1" ];then # 1-) Logstash (From Official Package)
    sudo dnf -vy remove java*
    sudo dnf -vy install java-11-openjdk-devel
    sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
    echo "[logstash-8.x]
name=Elastic repository for 8.x packages
baseurl=https://artifacts.elastic.co/packages/8.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md" > /etc/yum.repos.d/logstash.repo
    sudo dnf -vy install logstash
    sudo systemctl daemon-reload
    sudo systemctl start logstash
    sudo systemctl enable logstash
elif [ "$logstash_version" = "2" ];then # 2-) Logstash (Via Docker)
    sudo dnf -vy remove java*
    sudo dnf -vy install java-11-openjdk-devel
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
    sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    systemctl start docker
    systemctl enable docker
    docker_logstash=$(lynx -dump https://www.elastic.co/guide/en/logstash/current/docker.html | grep -iv "obtaining logstas" | grep -i "docker pull" | head -n 1)
    eval "$docker_logstash"
    docker_compose_version=$(lynx -dump https://github.com/docker/compose/releases | awk '/http/{print $2}' | grep -i ".tar.gz" \
    | head -n 1 | sed -E 's/.*\/v([0-9]+\.[0-9]+\.[0-9]+).*/\1/')
    sudo curl -L "https://github.com/docker/compose/releases/download/v$docker_compose_version/docker-compose-linux-$(uname -m)"\
    -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
elif [ "$logstash_version" = "3" ];then # 3-) Logstash (From .rpm file)
    sudo dnf -vy remove java*
    sudo dnf -vy install java-11-openjdk-devel
    latest_logstash_version=$(lynx -dump https://www.elastic.co/guide/en/logstash/current/docker.html | grep -iv "obtaining logstas" \
    | grep -i "docker pull" | head -n 1 | sed -E 's/.*logstash:([0-9]+\.[0-9]+\.[0-9]+).*/\1/')
    sudo rpm -Uvh https://artifacts.elastic.co/downloads/logstash/logstash-$latest_logstash_version-x86_64.rpm
    sudo systemctl restart logstash
    sudo systemctl enable logtash
elif [ "$logstash_version" = "4" ];then # 4-) Logstash Source File
    sudo mkdir -pv /root/Downloads/logstash
    sudo dnf -vy remove java*
    sudo dnf -vy install java-11-openjdk-devel
    wget -O /root/Downloads/logstash-8.3.2-linux-x86_64.tar.gz \
    https://artifacts.elastic.co/downloads/logstash/logstash-8.3.2-linux-x86_64.tar.gz
    tar -xvf /root/Downloads/logstash-8.3.2-linux-x86_64.tar.gz -C /root/Downloads/logstash --strip-components 1
    echo "Installation completed, folder under /root/Downloads/logstash/"
else
    echo "Out of options please choose between 1-3"
fi